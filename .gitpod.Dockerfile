FROM ubuntu:20.04
USER root
RUN apt update -qq && apt install sudo curl dnsutils hwloc htop nano -qq -y && bash -c "echo vm.nr_hugepages=1280 >> /etc/sysctl.conf"